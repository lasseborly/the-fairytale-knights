#The Fairytale Knights

######A simple front end website that served as nothing more than info site for a Guild Wars 2 guild.

##Technology

* ###[Bootstrap](http://getbootstrap.com/)

    Front-end framework used mainly for it's grid and responsive capabilities. 
***

* ###[jQueryp](http://jquery.com/)

    Most used JavaScript library. Dependency of Bootstraps JavaScript features.
***